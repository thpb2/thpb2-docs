# thpb2

This file was originally written by Peter Kim, author of The Hacker Playbook series.
The original may be found at https://github.com/cheetz/thp2/blob/master/setup.txt

This version of the Kali Lab uses - kali-linux-2019.3a-vbox-amd64.ova

---

The Hacker Playbook 2
http://www.amazon.com/dp/1512214566/

Since this book is based off of the Kali Linux platform, you can download the Kali Linux distro from: http://www.kali.org/downloads/. I highly recommend you download the VMware image (https://www.offensive-security.com/kali-linux-vmware-arm-image-download/) and download Virtual Player/VirtualBox. Remember that it will be a gz-compressed and tar archived file, so make sure to extract them first and load the vmx file.

# Setting Up Kali Linux

## Once Your Kali VM is Up and Running
- Log in with the default credentials `root:toor`
- Open a terminal
- Change the password
  - `passwd`
- Update the image
  - `apt-get update`
  - `apt-get dist-upgrade`
- Setup Metasploit database
  - `service postgresql start`
- Make postgresql database start on boot
  - `update-rc.d postgresql enable`
- Create and initialize the Metasploit database
  - `msfdb init`
- Change the hostname - Many network admins look for systems named Kali in logs like DHCP. It is best to follow the naming standard used by the company you are testing
  - `vim /etc/hostname`
    - Change the hostname (replace kali) and save
  - `vim /etc/hosts`
    - Change the hostname (replace kali) and save
  - reboot
- Create the opt directory
  - `mkdir /opt`
- Optional for Metasploit - Enable Logging
  - I list this as optional since logs get pretty big, but you have the ability to log every command and result from Metasploit’s Command Line Interface (CLI). This becomes very useful for bulk attack/queries or if your client requires these logs. If this is a fresh image, type msfconsole first and exit before configuring logging to create the .msf4 folder.
  - From a command prompt, type:
    - `echo “spool /root/msf_console.log” > /root/.msf4/msfconsole.rc`
  - Logs will be stored at `/root/msf_console.log`

## Tool Installation

### HTTPScreenShot
HTTPScreenshot is a tool for grabbing screenshots and HTML of large numbers of websites.
- `git clone https://gitlab.com/thpb2/httpscreenshot.git /opt/httpscreenshot`
- `cd /opt/httpscreenshot`
- `chmod +x install-dependencies.sh`
- `./install-dependencies.sh`

### SMBExec
A rapid psexec style attack with samba tools.
- `git clone https://gitlab.com/thpb2/smbexec.git /opt/smbexec`
- `git clone https://gitlab.com/thpb2/libesedb.git /opt/libesedb`
- `git clone https://gitlab.com/thpb2/ntdsxtract.git /opt/NTDSXtract`
- `wget https://gitlab.com/thpb2/dumpntds/raw/master/dshashes.py -O /opt/NTDSXtract/dshashes.py`
- `apt-get install automake autoconf autopoint gcc-mingw-w64-x86-64 libtool pkg-config passing-the-hash ruby-nokogiri ruby-libxml libxml2-dev libxslt1-dev`
- `cd /opt/libesedb`
- `./synclibs.sh`
- `./autogen.sh`
- `./configure`
- `make`
- `gem install bundler`
- Edit the file `/opt/smbexec/smbexec.yml`
  - `mingw: /usr/bin/x86_64-w64-mingw32-gcc`
  - `esedbexport: /opt/libesedb/esedbtools/esedbexport`
- `cd /opt/smbexec`
- `./install.sh`
- Select 1 - Debian/Ubuntu and derivatives
- Select all defaults

### Gitrob
Reconnaissance tool for GitHub organizations
- `apt-get install golang`
- `mkdir /opt/go /opt/go/src /opt/go/bin`
- Update the GoLang environmental path
  - `echo "export GOPATH=/opt/go" >> ~/.bashrc`
  - `echo "export PATH=$GOPATH/bin:$GOROOT/bin:$PATH" >> ~/.bashrc`
- Gitrob will need a Github access token in order to interact with the Github API.
  - `echo "export GITROB_ACCESS_TOKEN=deadbeefdeadbeefdeadbeefdeadbeefdeadbeef" >> ~/.bashrc`
- `source ~/.bashrc`
- `go get gitlab.com/thpb2/gitrob`

### CMSmap
CMSmap is a python open source CMS (Content Management System) scanner that automates the process of detecting security flaws
- `git clone https://gitlab.com/thpb2/cmsmap.git /opt/CMSmap`

### Printer Exploits
Contains a number of commonly found printer exploits
- `git clone https://gitlab.com/thpb2/praedasploit.git /opt/praedasploit`

### Discover Scripts
Custom bash scripts used to automate various pentesting tasks
- `git clone https://gitlab.com/thpb2/discover.git /opt/discover`
- `cd /opt/discover`
- `./update.sh`

### The Hacker Playbook 2 - Custom Scripts
A number of custom scripts written by Peter Kim for The Hacker Playbook 2.
- `git clone https://gitlab.com/thpb2/easy-p.git /opt/Easy-P`
- `git clone https://gitlab.com/thpb2/password_plus_one.git /opt/Password_Plus_One`
- `git clone https://gitlab.com/thpb2/powershell_popup.git /opt/PowerShell_Popup`
- `git clone https://gitlab.com/thpb2/icmpshock.git /opt/icmpshock`
- `git clone https://gitlab.com/thpb2/brutescrape.git /opt/brutescrape`
- `git clone https://www.gitlab.com/thepb2/reddit_xss.git /opt/reddit_xss`

### The Hacker Playbook 2 - Forked Versions
Forked versions of PowerSploit and Powertools used in the book. Make sure you clone your own repositories from the original sources.
- `git clone https://gitlab.com/thpb2/hp_powersploit.git /opt/HP_PowerSploit`
- `git clone https://gitlab.com/thpb2/hp_powertools.git /opt/HP_PowerTools`
- `git clone https://gitlab.com/thpb2/nishang.git /opt/nishang`

### NoSQLMap
A automated pentesting toolset for MongoDB database servers and web applications.
- `git clone https://gitlab.com/thpb2/nosqlmap.git /opt/NoSQLMap`

### Spiderfoot
Open Source Footprinting Tool
- `wget --no-check-certificate https://www.spiderfoot.net/files/spiderfoot-2.12.0-src.tar.gz`
- `mkdir /opt/spiderfoot`
- `tar xzvf spiderfoot-2.12.0-src.tar.gz -C /opt/spiderfoot --strip-components 1`
- `rm spiderfoot-2.12.0-src.tar.gz`
- `python -m pip install lxml`
- `python -m pip install netaddr`
- `python -m pip install M2Crypto`
- `python -m pip install cherrypy`
- `python -m pip install mako`

### WCE
Windows Credential Editor (WCE) is used to pull passwords from memory
  - `wget https://www.ampliasecurity.com/research/wce_v1_41beta_universal.zip`
  - `mkdir /opt/wce`
  - `unzip wce_v1_41beta_universal.zip -d /opt/wce`
  - `rm wce_v1_41beta_universal.zip`

### Mimikatz
Used for pulling cleartext passwords from memory, Golden Ticket, skeleton key and more
  - `wget https://github.com/gentilkiwi/mimikatz/releases/download/2.2.0-20190813/mimikatz_trunk.zip`
  - `mkdir /opt/mimikatz`
  - `unzip mimikatz_trunk.zip -d /opt/mimikatz`
  - `rm mimikatz_trunk.zip`

### Veil-Framework
A red team toolkit focused on evading detection. It currently contains Veil-Evasion for generating AV-evading payloads, Veil-Catapult for delivering them to targets, and Veil-PowerView for gaining situational awareness on Windows domains. Veil will be used to create a python based Meterpreter executable.
- `if [[ ! /opt/Veil ]]; then git clone https://gitlab.com/thpb2/veil.git /opt/Veil; fi`
- `./opt/Veil/`

### Fuzzing Lists (SecLists)
These are scripts to use with Burp to fuzz parameters
- `git clone https://gitlab.com/thpb2/seclists.git /opt/SecLists`

### Net-Creds Network Parsing 
Parse PCAP files for username/passwords
- `git clone https://gitlab.com/thpb2/net-creds.git /opt/net-creds`

### Firefox Add-ons
Web Developer Add-on: https://addons.mozilla.org/en-US/firefox/addon/web-developer/
- Tamper Data for FF Quantum: `https://addons.mozilla.org/en-US/firefox/addon/tamper-data-for-ff-quantum`
- Foxy Proxy: `https://addons.mozilla.org/en-US/firefox/addon/foxyproxy-standard`
- User Agent Switcher: `https://addons.mozilla.org/en-US/firefox/addon/????????????` <- find alt or revision

### Phishing (Optional):
- Phishing-Frenzy
  - `git clone https://gitlab.com/thpb2/phishing-frenzy.git /var/www/phishing-frenzy`
- Custom List of Extras
  - `git clone https://gitlab.com/thpb2/gitlist.git /opt/gitlist`

# Setting Up Windows

## Tool Installation

### HxD
- `https://mh-nexus.de/en/downloads.php?product=HxD20`

### Evade
- `https://www.securepla.net/antivirus-now-you-see-me-now-you-dont`

### Hyperion
- `http://nullsecurity.net/tools/binary.html`
  - Download / Install a Windows compiler `https://sourceforge.net/projects/mingw/`
  - Run "make" in the extracted Hyperion folder and you should have the binary.

### Metasploit
- `https://metasploit.com/`

### Vulnerability Scanner
  - Nessus `https://www.tenable.com/products/nessus/nessus-essentials` (Trial)
  - Nexpose `https://www.rapid7.com/info/nexpose-community/` (Trial)
  - Advanced IP Scanner - https://www.advanced-ip-scanner.com/news/index.php?ID=7713 (Free)

### Nmap
- `https://nmap.org/book/inst-windows.html`

### oclHashcat
- `https://hashcat.net/hashcat/`

### Cain and Abel
- `https://thpb2.s3-us-west-2.amazonaws.com/ca_setup.zip`

### Burp Suite Pro
- `https://portswigger.net/burp/communitydownload`

### Nishang
- `https://gitlab.com/thpb2/nishang.git`

### PowerSploit
- `https://gitlab.com/thpb2/PowerSploit`

### Firefix (Add-ons)
- Web Developer Add-on
- Tamper Data for FF Quantum: `https://addons.mozilla.org/en-US/firefox/addon/tamper-data-for-ff-quantum`
- Foxy Proxy: `https://addons.mozilla.org/en-US/firefox/addon/foxyproxy-standard`
- User Agent Switcher: `https://addons.mozilla.org/en-US/firefox/addon/????????????` <- find alt or revision
